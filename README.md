# Maven + Servlet + JSP + Embedded Tomcat

This is the base maven project we will used in class.

## Visual Studio Code and Java

We are going to be working with VSCode at class.

### Configuring VSCode + Java

To be able to ejecute Java projects in VSCode follow the next steps:

* Open VSCode
* Click on **Extensions (Ctrl + Shift + X)**.
* Search for **Java Extension Pack** and install it (It will install 6 modules: *Language Support for Java, Debugger, Java Test Runner, Maven for Java, Java Dependency Viewer and Visual Studio IntelliCode*).
* Once installed, some new tabs will show. Got to **Configure Java Runtime** tab.
* Probably it will show that no *JDK installation was detected*.
* If so, Go to **install** and click on **Download OpenJDK 17**.
  * ```OpenJDK17*.msi``` file will be downloaded.
  * Install it with default values.
  * Go back to VSCode and click on **Reload window** button.

### Installing maven and adding it to the Path

* If you don't have maven installed:
  * Visit https://maven.apache.org/download.cgi
  * Download **Binary zip archive**.
  * Unzip it somewhere (e.g.: C:\opt).
  * Open **View Advanced System Settings** (*Ver la configuración avanzada del sistema* in Spanish)-
  * ```Advanced > Environment Variables > System Variables > Path > Edit > New```
  * Add ```C:\opt\apache-maven-3.6.3\bin``` (or wherever your mvn binary has been unziped) and click on **Accept**.
  * Close any VSCode window that you have to apply all changes.

### Clone this repo and execute it

Now that we have all the needed elements installed, let's continue and start working:

* Create a folder called ```backend-workspace``` wherever you want.
* Open it.
* ```Right-click > Git Bash here``` and make a clone (It will open the forder using Git Bash, you should write the clone command as follows):

```console
MONDRAGON+aperez@DESKTOP-LQAQDCH MINGW64 ~/backend-workspace
$ git clone https://gitlab.com/mgep-web-engineering-1/javaee/00-maven-embedded-tomcat.git
Cloning into 'maven-embedded-tomcat'...
remote: Enumerating objects: 20, done.
remote: Counting objects: 100% (20/20), done.
remote: Compressing objects: 100% (15/15), done.
remote: Total 20 (delta 4), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (20/20), done.
Resolving deltas: 100% (4/4), done.
```

* Close Git Bash.
* Go to VSCode and ```File > Open Folder...``` and select the folder **00-maven-embedded-tomcat**. **WARNING** do not open backend-workspace, open each application in a VSCode instance.
* ```Terminal > New Terminal``` and execute this 2 commands (mvn package and mvn install):

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn package
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> mvn install
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  13.637 s
[INFO] Finished at: 2020-05-13T18:17:19+02:00
[INFO] ------------------------------------------------------------------------
...
```

* Right click on ```src > main > java> ... >main.java``` and click "Run Java". You will see the folowing:

```console
may. 13, 2020 6:18:11 P. M. org.apache.coyote.AbstractProtocol start
INFORMACIN: Starting ProtocolHandler ["http-nio-8080"]
```

* Opening http://localhost:8080 on a browser you should see "Hello WebEng1! [JSP]" and clicking on the link you should see "Hello WebEng1! [Servlet]".

You have compiled your first Java Web Application using Maven and embedded Tomcat!
```Ctrl + C``` and ```Y``` or ```S``` to stop the server.

* You can also run the application, being at 00-mvaven-embedded-tomcat folder executing the binary:

```console
PS C:\Users\aperez\backend-workspace\00-maven-embedded-tomcat> .\target\bin\maven_embedded_tomcat_launch.bat

or in linux

~/backend-workspace/00-maven-embedded-tomcat$ ./target/bin/maven_embedded_tomcat_launch

```

#### Debugger

It is very important to be able to debug the application so we can see what it is happening in the insides:

* Open ```src > main > java> ... > controller > HelloController.java``` and add a breakpoint inside ```doGet``` function.
* Right click on ```src > main > java > launch > Main.java``` and **Debug**.
* Open http://localhost:8080/hello in the browser, debuger should have stopped in the breakpoint.

## References

Based on [create-a-java-web-application-using-embedded-tomcat](https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat)
and
[maven-tomcat-embeded-webproject](https://github.com/richard937/maven-tomcat-embed-webproject)

### Other Interesting Links

* [Java in Visual Studio Code](https://code.visualstudio.com/docs/languages/java).
* [How to install maven in windows](https://mkyong.com/maven/how-to-install-maven-in-windows/).
* [VSCode + Java Getting started tutorial](https://code.visualstudio.com/docs/java/java-tutorial).

### Next

* Next [01-helloworld](https://gitlab.com/mgep-web-engineering-1/javaee/01-helloworld)
